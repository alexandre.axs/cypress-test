const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    //add the chromeWebSecurity: false, to solve the problem with the site 'www.saucedemo.com'. Sometimes the event 'load' didn't occours, so the test didn't reconize the page...
    //this solution was found here 'https://github.com/cypress-io/cypress/issues/21213'
    chromeWebSecurity: false,
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    baseUrl: 'https://www.saucedemo.com'
  },
});
