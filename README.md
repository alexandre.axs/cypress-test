# cypress-test


### 1. Install Cypress

[Follow these instructions to install Cypress.](https://on.cypress.io/installing-cypress)

### 2. Install all dependencies

```bahs
## to install all dependencies of the project
npm install
```

### 3. Run locally

To run locally this project you can run with the folowing commands

```bash
##to run with headless mode
npx run runHeadless

##to run with Headed mode
npx run runHeaded
```

### 4. Run in the pipeline

You can go to the menu **Build** then **Pipelines** and click in the button **Run Pipeline** to manualy star a new Build for the project

