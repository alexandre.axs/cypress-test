// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
Cypress.Commands.add('verifyEntries', (entries) => {
    cy.request('GET', 'https://api.publicapis.org/entries')
        .then((response) => {
            expect(response.status).to.eq(200)
            const authenticationEntries = response.body.entries.filter(entry => entry.Category === entries)
            for (var i = 0; i < authenticationEntries.length; i++) {
                var authenticationEntriesString = JSON.stringify(authenticationEntries[i]);
                cy.log(entries + ':', authenticationEntriesString)
            }
        })
})

Cypress.Commands.add('login', (username = 'standard_user', password = 'secret_sauce') => {
    cy.visit('/')
    cy.get('#user-name').type(username)
    cy.get('#password').type(password)
    cy.get('#login-button').click()
})

Cypress.Commands.add('verifyZtoA', () => {
    cy.get('[data-test="product_sort_container"]').select('za')
    cy.get('[class="inventory_item_name"]').eq(0).should("have.text", "Test.allTheThings() T-Shirt (Red)");
    cy.get('[class="inventory_item_name"]').eq(1).should("have.text", "Sauce Labs Onesie");
    cy.get('[class="inventory_item_name"]').eq(2).should("have.text", "Sauce Labs Fleece Jacket");
    cy.get('[class="inventory_item_name"]').eq(3).should("have.text", "Sauce Labs Bolt T-Shirt");
    cy.get('[class="inventory_item_name"]').eq(4).should("have.text", "Sauce Labs Bike Light");
    cy.get('[class="inventory_item_name"]').eq(5).should("have.text", "Sauce Labs Backpack");
})
